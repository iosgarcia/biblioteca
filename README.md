## DESAFIO DE PHP COM LARAVEL

O nome deste projeto é biblioteca e tem por objetivo apresentar os livros que fazer parte dessa biblioteca, para tanto, o usuário pode se cadastrar e, logo após, logar na aplicação a partir do seus
dados cadastrados.
Na parte interna do sistema o usuário pode cadastrar, editar e excluir um livro da biblioteca.
O usuário também pode acompanhar o clima da cidade de Belém, PA

## COMO EXECUTAR ESSE PROJETO

1) O usuário precisa possuir o ambiente do laravel devidamente instalado na sua máquina
2) O usuário precisa criar o banco de dados com o nome biblioteca
3) O próximo passo é acessar o diretório raiz do projeto e executar o comando composer install
4) O próximo passo é editar o arquivo .env.example com o seguintes dados:
    APP_NAME=Biblioteca
    DB_DATABASE=biblioteca
É preciso verificar se é necessário mas alterações nesse arquivo
5) O próximo passo é renomear o arquivo env.example para .env
6) O usuário precisa executar o comando php artisan migrate para realizar as migrations desse projeto
7) O usuário precisa executar o comando php artisan serve para subir a aplicação
8) É isso!

## LICENÇA

O framework Laravel é um software de código aberto licenciado sob a [MIT license](https://opensource.org/licenses/MIT).

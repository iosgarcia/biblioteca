<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Livros extends Model
{
    use HasFactory;

    protected $table = 'livros';

    public function lista() {
        $ordem = '';
        if (session('ordem_resultado') != null) {
            $ordem = session('ordem_resultado');
        } else {
            $ordem = session('ordem');
        }
        $db = $users = DB::table($this->table)
        ->orderBy('id', $ordem)
        ->paginate(4);;
        return $db;
    }

    public function exclui($id_livro)
    {
        DB::table($this->table)->where('id', $id_livro)->delete();
    }

    public function atualiza($id_livro, $dados) {
        DB::table($this->table)
            ->where('id', $id_livro)
            ->update($dados);
    }

    public function cadastra($dados) {
        DB::table($this->table)->insert($dados);
    }

    public function nomeLivro($nome) {
        DB::table($this->table)->where('titulo', $nome)->get();
    }

}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LivrosApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    
     public function procura() {
        $pesquisa = $this->input->post('phrase');
        print_r($pesquisa);
     }
    
     public function toArray($request)
    {
        return parent::toArray($request);
    }
}

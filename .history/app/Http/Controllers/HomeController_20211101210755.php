<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Livros as ModelsLivros;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $request->session()->put('ordem', 'ASC');
        $livros = new ModelsLivros();
        $qry = $livros->lista();
        $tempo = $this->consulta();
        return view('home', compact('qry', 'tempo'));
    }

    private function curl($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);

        return $resp;
    }
    public function consulta() {
        $con = $this->curl('https://api.hgbrasil.com/weather?woeid=455820');

        return json_decode($con); 
        
    }
}

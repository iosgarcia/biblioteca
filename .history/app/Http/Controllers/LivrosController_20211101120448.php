<?php

namespace App\Http\Controllers;

use App\Models\Livros as ModelsLivros;
use Illuminate\Http\Request;

class LivrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exclui(Request $request) {
        $id_livro = $request->input('id_livro');
        $livros   = new ModelsLivros();
        
        $livros->exclui($id_livro);
            
        return response()->json([
            'situacao' => 'success',
            'msg' => 'Operação realizada com sucesso'
        ]); 
    }

    public function edita(Request $request) {
        
        $valida = $request->validate([
            'titulo_livro' => 'required',
            'autor_livro'  => 'required',
            'descricao_livro'  => 'required',
            'num_pagina_livro' => 'required',
            'data_cadastro_livro' => 'required'
        ]
        );
        
    }

    public function messages(){

    return [
        'num_pagina.required' => 'A title is required',
        'titulo_livro.required'  => 'A message is required',
    ];
  }
}

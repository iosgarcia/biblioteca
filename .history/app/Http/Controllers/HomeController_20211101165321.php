<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Livros as ModelsLivros;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $request->session()->put('ordem', 'DESC');
        $livros = new ModelsLivros();
        $qry = $livros->lista();
        return view('home', compact('qry'));
    }
}

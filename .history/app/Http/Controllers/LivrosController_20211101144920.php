<?php

namespace App\Http\Controllers;

use App\Models\Livros as ModelsLivros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LivrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exclui(Request $request) {
        $id_livro = $request->input('id_livro');
        $livros   = new ModelsLivros();
        
        $livros->exclui($id_livro);
            
        return response()->json([
            'situacao' => 'success',
            'msg' => 'Operação realizada com sucesso'
        ]); 
    }

    protected function validaForm(Request $request) {
        
        $messages = [
            'num_paginas.required' => 'O campo de número de páginas do livro é um parâmetro obrigatório',
            'titulo.required' => 'O campo de titulo do livro é um parâmetro obrigatório',
            'autor.required'  => 'O campo de autor do livro é um parâmetro obrigatório',
            'descricao.required'  => 'O campo de descrição do livro é um parâmetro obrigatório',
        ];

        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'autor'  => 'required',
            'descricao'  => 'required',
            'num_paginas' => 'required',
            'data_cadastro' => 'required'
        ], $messages)->validate();

        
    }

    public function edita(Request $request) {
        $this->validaForm($request);  
        $livros   = new ModelsLivros();
        $id_livro = $request->input('id_livro');
        $dados = [
            'titulo' => $request->input('titulo'),
            'autor'  => $request->input('autor'),
            'descricao' => $request->input('titulo'),
        ];
        /* $livros->atualiza($id_livro, request()->except(['_token', 'op', 'id_livro'])); */
    }
}

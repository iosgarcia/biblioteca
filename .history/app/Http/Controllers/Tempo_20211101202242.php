<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

class Tempo extends Controller
{
    private function curl($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);

        return $resp;
    }
    public function auth() {
        $url = "https://api.hgbrasil.com/weather?key=32d770ca";
        $this->curl($url);
        $con = $this->curl('https://api.hgbrasil.com/weather?woeid=455820');

        return $con; 
        
    }
}

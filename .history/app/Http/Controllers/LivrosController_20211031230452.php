<?php

namespace App\Http\Controllers;

use App\Livros;
use App\Models\Livros as ModelsLivros;
use Illuminate\Http\Request;

class LivrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lista () {
        $livros = new ModelsLivros();
        print_r($livros->livros());

    }
}

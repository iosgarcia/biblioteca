<?php

namespace App\Http\Controllers;

use App\Models\Livros as ModelsLivros;
use Illuminate\Http\Request;

class LivrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exclui(Request $request) {
        $id_livro = $request->input('id_livro');
        $livros   = new ModelsLivros();
        
        if($livros->exclui($id_livro)) {
            return response()->json([
                'situacao' => 'success',
                'msg' => 'Operação realizada com sucesso'
            ]);
        } else {
            return response()->json([
                'situacao' => 'error',
                'msg' => 'Ocorreu um erro ao realizar a operação'
            ]);
        }
       
        
    }
}

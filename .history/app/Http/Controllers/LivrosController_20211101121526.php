<?php

namespace App\Http\Controllers;

use App\Models\Livros as ModelsLivros;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
class LivrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exclui(Request $request) {
        $id_livro = $request->input('id_livro');
        $livros   = new ModelsLivros();
        
        $livros->exclui($id_livro);
            
        return response()->json([
            'situacao' => 'success',
            'msg' => 'Operação realizada com sucesso'
        ]); 
    }

    public function edita(Request $request) {
        
        $rules = [
            'titulo_livro' => 'required',
            'autor_livro'  => 'required',
            'descricao_livro'  => 'required',
            'num_pagina_livro' => 'required',
            'data_cadastro_livro' => 'required'
        ];
        
        $messages = [
            'num_pagina_livro.required' => 'We need to know your e-mail address!',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages)->validate();

        
     
    }

    public function messages(){

    return [
        'num_pagina_livro.required' => 'A title is required',
        'titulo_livro.required'  => 'A message is required',
    ];
  }
}

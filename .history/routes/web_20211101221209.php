<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);

Auth::routes();

Route::get('/livros', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/livros/detalhes/{id_livro}', [App\Http\Controllers\LivrosController::class, 'detalhe'])->name('detalhe');
Route::post('/livros/exclui', [App\Http\Controllers\LivrosController::class, 'exclui'])->name('exclui');
Route::post('/livros/edita', [App\Http\Controllers\LivrosController::class, 'edita'])->name('edita');
Route::post('/livros/ordena', [App\Http\Controllers\LivrosController::class, 'ordena'])->name('ordena');
Route::post('/livros/procura', [App\Http\Controllers\LivrosController::class, 'procura'])->name('procura');
Route::post('/livros/auto', [App\Http\Controllers\LivrosController::class, 'auto'])->name('auto');

Route::get('/tempo', [App\Http\Controllers\Tempo::class, 'consulta'])->name('consulta');

$('input[name="data_cadastro"]').daterangepicker({
    singleDatePicker: true,
    startDate: moment().startOf('hour'),
    locale: {
        format: 'DD/MM/YYYY',
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "fromLabel": "De",
        "toLabel": "Até",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Dom",
            "Seg",
            "Ter",
            "Qua",
            "Qui",
            "Sex",
            "Sáb"
        ],
        "monthNames": [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"
        ],
        "firstDay": 0
    },
    opens: 'center',
    drops: 'auto',
    "autoApply": true
}, function (chosen_date) {
    $('input[name="data_cadastro_livro').val(chosen_date.format('DD/MM/YYYY'));
});   
$('#tabela_livros').DataTable({
    "paging": true,
    scrollX: true,
    scrollCollapse: true,
    "ordering": true,
    responsive: {
        details: {
            type: 'column',
            target: 'tr'
        }
    },
    "stateSave": true,
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ Resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Digite sua busca:",
        "oPaginate": {
            "sNext": "<i class='fa fa-arrow-circle-right' aria-hidden='true'>",
            "sPrevious": "<i class='fa fa-arrow-circle-left' aria-hidden='true'></i>",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "columnDefs": [
        {"orderable": false, "targets": [0, 1, 2]}
    ]
});

$('body').delegate('.btn-excluir', 'click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
        title: 'Você quer realmente excluir esse livro?',
        showDenyButton: true,
        confirmButtonText: 'Sim',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id_livro: id},
                type: "POST",
                dataType: 'json',
                url: 'livros/exclui',
                success: function (data) {
                    Swal.fire(
                        data['msg'],
                        '',
                        data['situacao']
                    );
                    const element = document.querySelector(".tbl-" + id);
                    element.classList.add('animate__animated', 'animate__fadeOut');
                    setTimeout(function(){ element.remove() }, 1500);
                }
            });
        }
    })
});

$('body').delegate('.btn-leia-mais', 'click', function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    var btn = $(".btn-" + id).text();
    if (btn === 'Ler Mais') {
        $(".btn-" + id).text("Ler Menos");
        $('.txt-curto-' + id).css('display', 'none');
        $('.mais-texto').css('display', 'none');
        $('.texto-' + id).css('display', 'block');
    } else {
        $(".btn-" + id).text("Ler Mais");
        $('.txt-curto-' + id).css('display', 'block');
        $('.mais-texto').css('display', 'none');
        $('.texto-' + id).css('display', 'none');
    }
});

$('body').delegate('.btn-editar', 'click', function (e) {
    e.preventDefault();
    $('#titulo_lvr').html('Edição do Livro <b>'+$(this).data('titulo') +'</b>');
    $("input[name='titulo']").val($(this).data('titulo'));
    $("input[name='autor']").val($(this).data('autor'));
    $("textarea[name='descricao']").text($(this).data('descricao'));
    $("input[name='num_paginas']").val($(this).data('num'));
    $("input[name='data_cadastro']").val($(this).data('cadastro'));
    $("input[name='op']").val('I');
    $("input[name='id_livro']").val($(this).data('id'));
    $('#modaledit').modal('show')
});  

$('body').delegate('.btn-salvar', 'click', function (e) {
    $(this).html('Salvando <i class="fas fa-spinner fa-spin"></i>');
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $("#form").serialize(),
        type: "POST",
        dataType: 'json',
        url: 'livros/edita',
        success: function (data) {
            $('.btn-salvar').html('Salvar <i class="fas fa-save"></i>');
            $("#success").append("<li class='alert alert-success'><i class='fas fa-check-circle'></i> "+data['msg']+"</li>")
            const element = document.querySelector(".alert-success");
           
            setTimeout(function(){ 
                element.classList.add('animate__animated', 'animate__fadeOut');
            }, 1500);
            setTimeout(function(){ 
                element.remove();
            }, 2500);
            setTimeout(function(){ 
                location.reload();
            }, 3000);
        },
        error: function(xhr, status, error) {
          $(this).html('Salvar <i class="fas fa-save"></i>');  
          $.each(xhr.responseJSON.errors, function (key, item) {
            $("#errors").append("<li class='alert alert-danger'>"+item+"</li>")
          });
        }
    });
});      
$('input[name="data_cadastro_livro"]').daterangepicker();
   
$('#tabela_livros').DataTable({
    "paging": true,
    scrollX: true,
    scrollCollapse: true,
    "ordering": true,
    responsive: {
        details: {
            type: 'column',
            target: 'tr'
        }
    },
    "stateSave": true,
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ Resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Digite sua busca:",
        "oPaginate": {
            "sNext": "<i class='fa fa-arrow-circle-right' aria-hidden='true'>",
            "sPrevious": "<i class='fa fa-arrow-circle-left' aria-hidden='true'></i>",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "columnDefs": [
        {"orderable": false, "targets": [0, 1, 2]}
    ]
});

$('body').delegate('.btn-excluir', 'click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
        title: 'Você quer realmente excluir esse livro?',
        showDenyButton: true,
        confirmButtonText: 'Sim',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id_livro: id},
                type: "POST",
                dataType: 'json',
                url: 'livros/exclui',
                success: function (data) {
                    Swal.fire(
                        data['msg'],
                        '',
                        data['situacao']
                    );
                    const element = document.querySelector(".tbl-" + id);
                    element.classList.add('animate__animated', 'animate__fadeOut');
                    setTimeout(function(){ element.remove() }, 1500);
                }
            });
        }
    })
});

$('body').delegate('.btn-leia-mais', 'click', function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    var btn = $(".btn-" + id).text();
    if (btn === 'Ler Mais') {
        $(".btn-" + id).text("Ler Menos");
        $('.txt-curto-' + id).css('display', 'none');
        $('.mais-texto').css('display', 'none');
        $('.texto-' + id).css('display', 'block');
    } else {
        $(".btn-" + id).text("Ler Mais");
        $('.txt-curto-' + id).css('display', 'block');
        $('.mais-texto').css('display', 'none');
        $('.texto-' + id).css('display', 'none');
    }
});

$('body').delegate('.btn-editar', 'click', function (e) {
    e.preventDefault();
    $('#titulo_lvr').html('Edição do Livro <b>'+$(this).data('titulo') +'</b>');
    $("input[name='titulo_livro']").val($(this).data('titulo'));
    $("input[name='autor_livro']").val($(this).data('autor'));
    $("textarea[name='descricao_livro']").text($(this).data('descricao'));
    $("input[name='num_pagina_livro']").val($(this).data('num'));
    $('#modaledit').modal('show')
});    
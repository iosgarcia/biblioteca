$('#tabela_livros').DataTable({
    "paging": true,
    scrollX: true,
    scrollCollapse: true,
    "ordering": true,
    responsive: {
        details: {
            type: 'column',
            target: 'tr'
        }
    },
    "stateSave": true,
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ Resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Digite sua busca:",
        "oPaginate": {
            "sNext": "<i class='fa fa-arrow-circle-right' aria-hidden='true'>",
            "sPrevious": "<i class='fa fa-arrow-circle-left' aria-hidden='true'></i>",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "columnDefs": [
        {"orderable": false, "targets": [0, 1, 2]}
    ]
});

$('body').delegate('.btn-excluir', 'click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
        title: 'Você quer realmente excluir esse livro?',
        showDenyButton: true,
        confirmButtonText: 'Sim',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = $(this).data('url');
        }
    })
});
$('#tabela_livros').DataTable({
    "paging": true,
    scrollX: true,
    scrollCollapse: true,
    "ordering": true,
    responsive: {
        details: {
            type: 'column',
            target: 'tr'
        }
    },
    "stateSave": true,
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ atÃ© _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 atÃ© 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ Resultados por pÃ¡gina",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Digite sua busca:",
        "oPaginate": {
            "sNext": "<i class='fa fa-arrow-circle-right' aria-hidden='true'>",
            "sPrevious": "<i class='fa fa-arrow-circle-left' aria-hidden='true'></i>",
            "sFirst": "Primeiro",
            "sLast": "Ãšltimo"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "Profissionais/lista",
        "type": "POST"

    },
    "columnDefs": [
        {"orderable": false, "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8]}
    ]
});
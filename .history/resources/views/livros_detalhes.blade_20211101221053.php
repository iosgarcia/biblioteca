@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row livros">
            <div class="col-md-12 col-sm-12">
                    <div class="row">
                        @if ($qry->isEmpty())
                        @else
                        @foreach ($qry as $livros)
                        <div class="col-md-6 col-sm-12 tbl-{{$livros->id}} tbl-lvr">
                            <div class="card lvr-card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $livros->titulo }}</h5>
                                    <h6 class="card-subtitle mb-2">Autor: {{ $livros->autor }}</h6>
                                    <p class="card-text">
                                        <span class="txt-curto-{{$livros->id}}">
                                            {{ mb_strimwidth($livros->descricao, 0, 200, "...") }}
                                        </span>
                                        <span class="mais-texto texto-{{$livros->id}}">
                                            {{ $livros->descricao }}
                                        </span>
                                        <p><span class="badge badge-success">Data de Cadastro: </span> {{(new \DateTime($livros->data_cadastro))->format('d/m/Y')}} <i class="far fa-calendar"></i></p>
                                        <button type="button" data-id="{{ $livros->id }}" class="btn btn-info btn-leia-mais btn-{{ $livros->id }}"><i class="fas fa-plus-circle"></i> DETALHES</button>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <button type="button" data-id="{{ $livros->id }}" class="btn btn-danger btn-excluir"><i class="fas fa-trash"></i>
                                                Excluir</button>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <button type="button" data-id="{{ $livros->id }}" class="btn btn-primary btn-editar" data-titulo="{{ $livros->titulo }}" data-descricao="{{ $livros->descricao }}" data-num="{{ $livros->num_paginas }}" data-autor="{{ $livros->autor }}" data-cadastro="{{ date('d/m/Y',  strtotime($livros->data_cadastro )) }}"><i class="fas fa-edit"></i>
                                                Editar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
            </div>
        </div>
        @include('layouts.modal')
    </div>
@endsection

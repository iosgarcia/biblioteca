@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row livros">
            <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="card lvr-card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $dados[0]->titulo}}</h5>
                                <h6 class="card-subtitle mb-2">Autor: {{  $dados[0]->autor }}</h6>
                                <p class="card-text">
                                    <span class="txt-curto-{{$livros->id}}">
                                        {{ $livros->descricao }}
                                    </span>
                                    <p><span class="badge badge-success">Data de Cadastro: </span> {{(new \DateTime($livros->data_cadastro))->format('d/m/Y')}} <i class="far fa-calendar"></i></p>
                                </p>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        @include('layouts.modal')
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row livros">
            <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="card lvr-card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $dados[0]->titulo}}</h5>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        @include('layouts.modal')
    </div>
@endsection

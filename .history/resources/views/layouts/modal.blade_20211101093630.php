<div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="/profile">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" name="titulo_livro" placeholder="Digite o nome do livro" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="descricao_livro" rows="3" placeholder="Digite a descrição do livro"></textarea>
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" name="num_pagina_livro" placeholder="Digite o número de páginas do livro" required>
                </div>
                <button type="submit" class="btn btn-primary">Procurar <i class="fas fa-search"></i></button>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="titulo_lvr"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="errors"></div>
            <div id="success"></div>
            <form id="form">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="titulo" placeholder="Digite o nome do livro" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="autor" placeholder="Digite o nome do autor do livro" required>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <textarea class="form-control" name="descricao" rows="3" placeholder="Digite a descrição do livro"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-3">
                        <div class="form-group">
                            <input type="tel" class="form-control" name="num_paginas number" placeholder="Digite o número de páginas do livro" required>
                            <span id="num_pagina_livro_error"></span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" name="data_cadastro" placeholder="Digite a data de cadastro do livro" required>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="op">
                <input type="hidden" name="id_livro">
                <button type="submit" class="btn btn-primary btn-salvar">Salvar <i class="fas fa-save"></i></button>
            </form>
        </div>
      </div>
    </div>
  </div>
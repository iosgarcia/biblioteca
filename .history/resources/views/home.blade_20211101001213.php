@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row temperatura">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('Temperatura') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Em Belém, 28°C') }}
                </div>
            </div>
        </div>
    </div>
    <div class="row livros">
        @foreach ($qry as $livros)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title">{{ $livros->titulo}}</h5>
                  <h6 class="card-subtitle mb-2">Autor: {{ $livros->autor}}</h6>
                  <p class="card-text">{{ $livros->descricao}}</p>
                  <div class="row">
                      <div class="col-md-6">
                        <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i> Excluir</button>
                      </div>
                      <div class="col-md-6">
                        <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</button>
                      </div>
                  </div>
                 
                </div>
              </div>
            {{-- <table id="tabela_livros" class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>TITULO</th>
                        <th>DESCRIÇÃO</th>
                        <th>AUTOR</th>
                        <th>NÚMERO DE PÁGINAS</th>
                        <th>DATA DE CADASTRO</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($qry as $livros)
                    <tr>
                        <td>{{ $livros->titulo}}</td>
                        <td></td>
                        <td>{{ $livros->autor}}</td>
                        <td>{{ $livros->num_paginas}}</td>
                        <td>{{ $livros->data_cadastro}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table> --}}
        </div>
        @endforeach
    </div>
</div>
@endsection

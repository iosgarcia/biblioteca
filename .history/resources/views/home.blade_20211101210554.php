@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row livros">
            <div class="col-md-8 col-sm-12">
                    <div class="row">
                        @if ($qry->isEmpty())
                        <h5>Nenhum resultado encontrado</h5>    
                        @else
                        @foreach ($qry as $livros)
                        <div class="col-md-6 col-sm-12 tbl-{{$livros->id}} tbl-lvr">
                            <div class="card lvr-card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $livros->titulo }}</h5>
                                    <h6 class="card-subtitle mb-2">Autor: {{ $livros->autor }}</h6>
                                    <p class="card-text">
                                        <span class="txt-curto-{{$livros->id}}">
                                            {{ mb_strimwidth($livros->descricao, 0, 200, "...") }}
                                        </span>
                                        <span class="mais-texto texto-{{$livros->id}}">
                                            {{ $livros->descricao }}
                                        </span>
                                        <p><span class="badge badge-success">Data de Cadastro: </span> {{(new \DateTime($livros->data_cadastro))->format('d/m/Y')}} <i class="far fa-calendar"></i></p>
                                        <button type="button" data-id="{{ $livros->id }}" class="btn btn-info btn-leia-mais btn-{{ $livros->id }}"><i class="fas fa-plus-circle"></i> DETALHES</button>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <button type="button" data-id="{{ $livros->id }}" class="btn btn-danger btn-excluir"><i class="fas fa-trash"></i>
                                                Excluir</button>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <button type="button" data-id="{{ $livros->id }}" class="btn btn-primary btn-editar" data-titulo="{{ $livros->titulo }}" data-descricao="{{ $livros->descricao }}" data-num="{{ $livros->num_paginas }}" data-autor="{{ $livros->autor }}" data-cadastro="{{ date('d/m/Y',  strtotime($livros->data_cadastro )) }}"><i class="fas fa-edit"></i>
                                                Editar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-md-12 col-sm-12">
                            {{$qry->links()}}
                        </div>
                        @endif
                        
                    </div>
                    
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card">
                    <div class="card-header">{{ __('O que você está procurando?') }}</div>
                    <div class="card-body">
                        <form method="POST" action="livros/procura">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control procura-input" placeholder="Digite o nome do livro" required name="procurar">
                            </div>
                            <button type="submit" class="btn btn-primary">Procurar <i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="card filtro">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <button type="button" class="btn btn-primary btn-cad">CADASTRAR LIVROS</button>
                            </div>
                        </div>
                        <div class="row filter">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary btn-ordena" data-ordem="DESC" data-toggle="tooltip" data-placement="top" title="ÚLTIMOS LIVROS"><i class="fas fa-sort-amount-down"></i></button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary btn-ordena" data-ordem="ASC" data-toggle="tooltip" data-placement="top" title="PRIMEIROS LIVROS"><i class="fas fa-sort-amount-up"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">{{ __('Temperatura') }}</div>
                    <div class="card-body">
                        {{print_r($tempo)}}
                        {{-- <p id="loading">Obtendo Dados <i class="fas fa-spinner fa-spin"></i></p> --}}
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h1 id="temp"></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.modal')
    </div>
@endsection

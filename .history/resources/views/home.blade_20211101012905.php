@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row livros">
            <div class="col-md-8 col-sm-12">
                    <div class="row">
                        @foreach ($qry as $livros)
                        <div class="col-md-6 col-sm-12 tbl-{{$livros->id}}">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $livros->titulo }}</h5>
                                    <h6 class="card-subtitle mb-2">Autor: {{ $livros->autor }}</h6>
                                    <p class="card-text">{{ $livros->descricao }}</p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <button type="button" data-id="{{ $livros->id }}" class="btn btn-danger btn-excluir"><i class="fas fa-trash"></i>
                                                Excluir</button>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <button type="button" data-id="{{ $livros->id }}" class="btn btn-primary btn-editar"><i class="fas fa-edit"></i>
                                                Editar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-md-12 col-sm-12">
                            {{$qry->links()}}
                        </div>
                    </div>
                    
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card">
                    <div class="card-header">{{ __('O que você está procurando?') }}</div>
                    <div class="card-body">
                        <form method="POST" action="/profile">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Digite o nome do livro" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Procurar <i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="card filtro">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        <button type="button" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        <button type="button" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        <button type="button" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">{{ __('Temperatura') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        {{ __('Em Belém, 28°C') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row temperatura">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('Temperatura') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Em Belém, 28°C') }}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="tabela_livros" class="table table-striped">
                {{print_r($livros)}}
                <thead class="thead-dark">
                    <tr>
                        <th>TITULO</th>
                        <th>DESCRIÇÃO</th>
                        <th>AUTOR</th>
                        <th>NÚMERO DE PÁGINAS</th>
                        <th>DATA DE CADASTRO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Row 1 Data 1</td>
                        <td>Row 1 Data 2</td>
                    </tr>
                    <tr>
                        <td>Row 2 Data 1</td>
                        <td>Row 2 Data 2</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

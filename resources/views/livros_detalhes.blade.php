@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row livros">
            <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="card lvr-card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $dados[0]->titulo}}</h5>
                                <h6 class="card-subtitle mb-2">Autor: {{  $dados[0]->autor }}</h6>
                                <p class="card-text">
                                    <span class="txt-curto-{{$dados[0]->id}}">
                                        {{ $dados[0]->descricao }}
                                    </span>
                                    <p><span class="badge badge-success">Data de Cadastro: </span> {{(new \DateTime($dados[0]->data_cadastro))->format('d/m/Y')}} <i class="far fa-calendar"></i></p>
                                </p>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                        <button type="button" data-id="{{ $dados[0]->id }}" class="btn btn-danger btn-excluir"><i class="fas fa-trash"></i></button>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <button type="button" data-id="{{ $dados[0]->id }}" class="btn btn-primary btn-editar" data-titulo="{{ $dados[0]->titulo }}" data-descricao="{{ $dados[0]->descricao }}" data-num="{{ $dados[0]->num_paginas }}" data-autor="{{ $dados[0]->autor }}" data-cadastro="{{ date('d/m/Y',  strtotime($dados[0]->data_cadastro )) }}"><i class="fas fa-edit"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        @include('layouts.modal')
    </div>
@endsection
